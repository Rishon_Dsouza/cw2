#ifndef _MAP_H
#define _MAP_H

/*
 * book.h
 * Author: M00697094

 * Brief: Implementation of an open-addressing hashmap using linear probing.
 *
 * [0] http://en.wikipedia.org/wiki/Open_addressing

 * Created: 28/3/2021
 * Updated: 28/4/2021
 */

#include <functional>
#include <optional>
#include <type_traits>
#include <vector>

/**
 * @brief Hashmap implementation that uses linear probing.
 * @note based on Dr. Barry hashmap header file
 * @tparam K Type of the keys.
 * @tparam V Type of the values.
 */
template <typename K, typename V>
class map {
    struct Record {
        K key;
        V value;
        bool has_value;

        Record() { has_value = false; }
        Record(K key, V value) {
            this->key = key;
            this->value = value;
            this->has_value = true;
        }
    };

    size_t capacity;
    size_t quantity;
    Record* internal_array;

    size_t find_slot(const K&);
    void resize(size_t new_capacity);

   public:
    map();
    ~map();

    std::optional<V> lookup(const K&);
    bool empty();
    bool contains(const K&);
    void insert(const K&, const V&);
    void remove(const K&);
    V& get(const K&);
    V& operator[](const K&);
    size_t size();
    size_t get_capacity();
    std::vector<K> keys();
    void clear();
};

template <typename K, typename V>
size_t map<K, V>::find_slot(const K& key) {
    size_t index = std::hash<K>{}(key) % capacity;

    while (internal_array[index].has_value &&
           internal_array[index].key != key) {
        index = (index + 1) % capacity;
    }

    return index;
}

template <typename K, typename V>
std::optional<V> map<K, V>::lookup(const K& key) {
    size_t index = find_slot(key);

    if (internal_array[index].has_value) {
        return internal_array[index].value;
    }

    return std::nullopt;
}

template <typename K, typename V>
void map<K, V>::resize(size_t new_capacity) {
    size_t old_capacity = this->capacity;
    Record* old_array = this->internal_array;

    this->quantity = 0;
    this->capacity = new_capacity;
    this->internal_array = new Record[new_capacity];

    for (size_t i = 0; i < old_capacity; i++) {
        if (old_array[i].has_value) {
            insert(old_array[i].key, old_array[i].value);
        }
    }

    delete[] old_array;
}

/**
 * @brief Makes an empty map object.
 */
template <typename K, typename V>
map<K, V>::map() {
    static_assert(std::is_default_constructible<K>::value,
                  "Map requires the key's type to have a default constructor.");
    static_assert(
        std::is_default_constructible<V>::value,
        "Map requires the value's type to have a default constructor.");
    this->quantity = 0;
    this->capacity = 128;
    this->internal_array = new Record[this->capacity];
}

/**
 * @brief Destroy the map object. Freeing any memory taken up.
 */
template <typename K, typename V>
map<K, V>::~map() {
    delete[] this->internal_array;
}

template <typename K, typename V>
bool map<K, V>::empty() {
    return this->quantity == 0;
}

/**
 * @brief Checks if the provided key is in the map object.
 *
 * @param key The key being searched for.
 * @return true If the key is found within the map.
 * @return false If the key wasn't found within the map.
 */
template <typename K, typename V>
bool map<K, V>::contains(const K& key) {
    return lookup(key) != std::nullopt;
}

/**
 * @brief Inserts a new value into the map at the provided key. Note that it
 * will override the value if the key already exists within the map object.
 *
 * @param key The key at which the value should be inserted.
 * @param value The value to be inserted.
 */
template <typename K, typename V>
void map<K, V>::insert(const K& key, const V& value) {
    size_t index = find_slot(key);

    // if the key is in our map just overwrite the value
    if (internal_array[index].has_value) {
        internal_array[index].value = value;
    }

    // if our table is getting close to being full
    if (quantity > (capacity / 2) + 1) {
        resize(2 * capacity);
    }

    // have to find index again, since it could have changed after resizing
    index = find_slot(key);
    internal_array[index] = Record(key, value);

    quantity++;
}

template <typename K, typename V>
void map<K, V>::remove(const K& key) {
    if (!contains(key)) {
        return;
    }

    size_t i = find_slot(key);
    internal_array[i] = Record();
    quantity--;

    size_t j = i;
    while (true) {
        internal_array[i] = Record();

    r2:
        j = (j + 1) % capacity;
        if (!internal_array[j].has_value) {
            break;
        }

        size_t k = std::hash<K>{}(internal_array[j].key) % capacity;
        if ((i <= j) ? ((i < k) && (k <= j)) : ((i < k) || (k <= j))) {
            goto r2;
        }

        internal_array[i] = internal_array[j];
        i = j;
    }
}

/**
 * @brief Get's the value at the provided key. Throws a `runtime_error` error
 * if the key is not found within the map. Make sure to check with `.contains()`
 * before using `get()`.
 *
 * @param key The key being looked up.
 * @return V& A reference to the found value.
 */
template <typename K, typename V>
V& map<K, V>::get(const K& key) {
    size_t index = find_slot(key);

    /*
        similar to the STL we throw when using `get()` but
        silently create the value when using `operator[]`
    */
    if (!internal_array[index].has_value) {
        throw std::runtime_error("Lookup on key that does not exist.");
    }

    return internal_array[index].value;
}

/**
 * @brief Get's the value at the provided key, or creates a value at the
 * provided key if it does not exist.
 *
 * @param key The key being looked up.
 * @return V& A reference to the found/created value.
 */
template <typename K, typename V>
V& map<K, V>::operator[](const K& key) {
    size_t index = find_slot(key);

    if (!internal_array[index].has_value) {
        quantity++;
        internal_array[index] = Record(key, V());
    }

    return internal_array[index].value;
}

template <typename K, typename V>
std::vector<K> map<K, V>::keys() {
    std::vector<K> result;

    for (size_t i = 0; i < capacity; i++) {
        if (internal_array[i].has_value) {
            result.push_back(internal_array[i].key);
        }
    }

    return result;
}

/**
 * @brief Returns the number of key-value pairs stored within this map.
 */
template <typename K, typename V>
size_t map<K, V>::size() {
    return quantity;
}

/**
 * @brief Returns the number of key-value pairs that can be stored before the
 * map will have to grow.
 */
template <typename K, typename V>
size_t map<K, V>::get_capacity() {
    return capacity;
}

/**
 * @brief Removes all elements from the map.
 */
template <typename K, typename V>
void map<K, V>::clear() {
    for (size_t i = 0; i < capacity; i++) {
        internal_array[i] = Record();
    }
    this->quantity = 0;
}

#endif
