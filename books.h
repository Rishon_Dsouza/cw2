/*
 * Author: M00737996
 * Created: 14/3/2021
 * Last Updated: 7/2/2021
 */
#ifndef BOOKS_H
#define BOOKS_H
#include <string>

/**
 * @brief An object class of a book
 * @param title the title of the book
 * @param author the writer of the book
 * @param isbn identification string of the book
 * @param qty quantity of the book in stock
 */
class Book {
   protected:
    std::string title;
    std::string author;
    std::string isbn;
    int qty;

   public:
    Book(std::string title, std::string author, std::string isbn, int qty);
    Book();
    std::string gettitle();
    std::string getauthor();
    std::string getisbn();
    int getqty();
    void settitle(std::string title);
    void setauthor(std::string author);
    void setisbn(std::string isbn);
    void setqty(int qty);
};
#endif