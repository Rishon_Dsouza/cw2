/*
 * Author: M00737996
 * Created: 14/1/2021
 * Last Updated: 7/2/2021
 */
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "books.h"
#include "hashmap.h"

/*
 *@brief adds books to the hashmap
 *@param
 *@return none
 */
void add_book(map<std::string, Book>& library, const std::string& path) {
    std::string title;
    std::cout << "Enter book title:" << std::endl;
    std::cin >> title;
    if (library.contains(title))
        std::cout << "This book already exists" << std::endl;

    std::string author;
    std::cout << "Enter author name:" << std::endl;
    std::cin >> author;

    std::string isbn;
    std::cout << "Enter an isbn:" << std::endl;
    std::cin >> isbn;

    int quantity;
    std::cout << "Enter quantity" << std::endl;
    std::cin >> quantity;

    library.insert(title, Book(title, author, isbn, quantity));
    std::cout << "The book" << title << "has been added." << std::endl;
}

/*
 *@brief Adds specified quanitty to vector of chosen item
 *@param object pointer id and quantity requirements
 *@return none
 */
void restock_items(std::vector<Item*>& vec) {
    int pck_id, pck_quantity;
    bool id_valid = false;
    bool quantity_flag = false;
    while (!id_valid) {
        std::cout << "Please enter the item ID " << std::endl;
        std::cin >> pck_id;
        for (Item* item : vec) {
            if (item->getid() == pck_id) {
                id_valid = true;
            }
        }
        std::cout << "Please enter the quantity of the item " << std::endl;
        std::cin >> pck_quantity;
        for (Item* item : vec) {
            if ((item->getquantity() > pck_quantity) ||
                (item->getquantity() == pck_quantity)) {
                quantity_flag = true;
            }
        }
        for (Item* item : vec) {
            if (item->getid() == pck_id) {
                item->setquantity(item->getquantity() + pck_quantity);
            }
        }

        if (!id_valid) {
            std::cout << "Given ID does not exist" << std::endl;
        }
        if (!quantity_flag) {
            std::cout << "Entered quantity exceeds the amount in stock"
                      << std::endl;
        }
    }  // let the user know and restart
}
/*
 *@brief sets quantity to new quantity of chosen item
 *@param object pointer id and quantity requirements
 *@return none
 */
void update_stock(std::vector<Item*>& vec) {
    int pck_id, pck_quantity;
    bool id_valid = false;
    bool quantity_flag = false;
    while (!id_valid) {
        std::cout << "Please enter the item ID " << std::endl;
        std::cin >> pck_id;
        for (Item* item : vec) {
            if (item->getid() == pck_id) {
                id_valid = true;
            }
        }
        std::cout << "Please enter the new quantity of the item " << std::endl;
        std::cin >> pck_quantity;
        for (Item* item : vec) {
            if ((item->getquantity() > pck_quantity) ||
                (item->getquantity() == pck_quantity)) {
                quantity_flag = true;
            }
        }
        for (Item* item : vec) {
            if (item->getid() == pck_id) {
                item->setquantity(pck_quantity);
            }
        }

        if (!id_valid) {
            std::cout << "Given ID does not exist" << std::endl;
        }
        if (!quantity_flag) {
            std::cout << "Entered quantity exceeds the amount in stock"
                      << std::endl;
        }
    }  // let the user know and restart
}
/*
 *@brief adds new item to the vector
 *@param object pointer
 *@return none
 */
void add_items(std::vector<Item*>& vec) {
    int choice;

    std::cout << "Enter a number from 1-4 to choose type of item" << std::endl;
    std::cout << "1. CD" << std::endl;
    std::cout << "2. DVD" << std::endl;
    std::cout << "3. Books" << std::endl;
    std::cout << "4. Magezine" << std::endl;
    std::cin >> choice;
    std::string pck_title, pck_album, pck_author, pck_isbn;
    int pck_year, pck_quantity, pck_issue, pck_id;
    float pck_price;
    /*temporarily asking user for id for testing,
    will later implement auto id genereation*/
    if (choice == 1) {
        std::cout << "Enter an ID: ";
        std::cin >> pck_id;
        std::cout << std::endl;
        std::cout << "Enter a title: ";
        std::cin.ignore();
        getline(std::cin, pck_title);
        std::cout << std::endl;
        std::cout << "Enter a album: ";
        getline(std::cin, pck_album);
        std::cout << std::endl;
        std::cout << "Enter a year: ";
        std::cin >> pck_year;
        std::cout << std::endl;
        std::cout << "Enter a price: ";
        std::cin >> pck_price;
        std::cout << std::endl;
        std::cout << "Enter the quantity: ";
        std::cin >> pck_quantity;
        std::cout << std::endl;
        vec.push_back(new CD(pck_id, pck_title, pck_album, pck_year, pck_price,
                             pck_quantity));

    } else if (choice == 2) {
        std::cout << "Enter an ID: ";
        std::cin >> pck_id;
        std::cout << std::endl;
        std::cout << "Enter a title: ";
        std::cin.ignore();
        getline(std::cin, pck_title);
        std::cout << std::endl;
        std::cout << "Enter a year: ";
        std::cin >> pck_year;
        std::cout << std::endl;
        std::cout << "Enter a price: ";
        std::cin >> pck_price;
        std::cout << std::endl;
        std::cout << "Enter the quantity: ";
        std::cin >> pck_quantity;
        std::cout << std::endl;
        vec.push_back(
            new DVD(pck_id, pck_title, pck_year, pck_price, pck_quantity));

    } else if (choice == 3) {
        std::cout << "Enter an ID: ";
        std::cin >> pck_id;
        std::cout << std::endl;
        std::cout << "Enter a title: ";
        std::cin.ignore();
        getline(std::cin, pck_title);
        std::cout << std::endl;
        std::cout << "Enter an author: ";
        getline(std::cin, pck_author);
        std::cout << std::endl;
        std::cout << "Enter a isbn: ";
        getline(std::cin, pck_isbn);
        std::cout << std::endl;
        std::cout << "Enter a price: ";
        std::cin >> pck_price;
        std::cout << std::endl;
        std::cout << "Enter the quantity: ";
        std::cin >> pck_quantity;
        std::cout << std::endl;
        vec.push_back(new Book(pck_id, pck_title, pck_author, pck_isbn,
                               pck_price, pck_quantity));

    } else if (choice == 4) {
        std::cout << "Enter an ID: ";
        std::cin >> pck_id;
        std::cout << std::endl;
        std::cout << "Enter a title: ";
        std::cin.ignore();
        getline(std::cin, pck_title);
        std::cout << std::endl;
        std::cout << "Enter an issue: ";
        std::cin >> pck_issue;
        std::cout << std::endl;
        std::cout << "Enter a isbn: ";
        std::cin.ignore();
        getline(std::cin, pck_isbn);
        std::cout << std::endl;
        std::cout << "Enter a price: ";
        std::cin >> pck_price;
        std::cout << std::endl;
        std::cout << "Eneter the quantity: ";
        std::cin >> pck_quantity;
        std::cout << std::endl;
        std::cout << "Enter a year: ";
        std::cin >> pck_year;
        std::cout << std::endl;
        vec.push_back(new Magezine(pck_id, pck_title, pck_issue, pck_year,
                                   pck_isbn, pck_price, pck_quantity));
    }
}
/*
 *@brief writes the vector to the file
 *@note must be used after modifying stock vector
 *@param object pointer
 *@return none
 */
void push_changes(std::vector<Item*>& vec) {
    std::string to_write;
    for (Item* item : vec) {
        for (const std::string& field : item->list()) {
            to_write.append(field + ",");
        }
        to_write.pop_back();
        to_write.append("\n");
    }
    write_file("./stock-data.txt", to_write);
}
int main() {
    int menu_option;
    std::vector<Item*> temp_store;
    while (true) {  // while loop so user is always prompted
        std::cout << "Select the option number and -1 to quit" << std::endl;
        std::cout << "1- Sell Item" << std::endl;
        std::cout << "2- Restock Item" << std::endl;
        std::cout << "3- Add Item" << std::endl;
        std::cout << "4- Update Stock" << std::endl;
        std::cout << "5- View Sales Report" << std::endl;
        std::cout << "6- Save/Push changes to file" << std::endl;
        std::cin >> menu_option;

        if (menu_option == 1) {
            sell_item(temp_store);
        } else if (menu_option == 2) {
            restock_items(temp_store);
        } else if (menu_option == 3) {
            add_items(temp_store);
        } else if (menu_option == 4) {
            update_stock(temp_store);
        } else if (menu_option == 5) {
            std::cout << "option 5" << std::endl;
        } else if (menu_option == 6) {
            std::cout << "Filler";
        } else if (menu_option == -1) {
            break;
        } else {
            std::cout << "Invalid option" << std::endl;
        }
    }
}