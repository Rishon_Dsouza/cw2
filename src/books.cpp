/*
 * Author: M00737996
 * Created: 14/1/2021
 * Last Updated: 7/2/2021
 */
#include "books.h"

#include <iostream>

Book::Book(std::string title, std::string author, std::string isbn, int qty)
    : title(title), author(author), isbn(isbn), qty(qty) {}

std::string Book::gettitle() { return this->title; };
std::string Book::getauthor() { return this->author; };
std::string Book::getisbn() { return this->isbn; };
int Book::getqty() { return this->qty; };
void Book::settitle(std::string title) { this->title = title; };
void Book::setauthor(std::string author) { this->author = author; };
void Book::setisbn(std::string isbn) { this->isbn = isbn; };
void Book::setqty(int qty) { this->qty = qty; };
